<?php
    namespace app\helpers;

    class ModelTransformer
    {
        /**
         * Transforms model objects into associative arrays of 'id' as key and 'name' as value.  
         */
        public static function mapIdsToNames($models)
        {
            $result = [];
            foreach ($models as $model) {
                $result[intval($model["id"])] = $model["name"];
            }
            return $result;
        }
    }