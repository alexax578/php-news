<?php
    namespace app\helpers;

    use yii\helpers\StringHelper;

    class ArticleHelper
    {
        /**
         * Finds and returns the first 40 words of the article.
         * @return string
         */
        public static function extractIntroduction($html)
        {
            $start = strpos($html, '<p>') + 3;
            $article = substr($html, $start);
            return StringHelper::truncateWords($article, 40, '...', true);
        }
    }