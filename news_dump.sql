-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: news
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `articles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `author_id` int NOT NULL,
  `category_id` int NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-article-author_id` (`author_id`),
  KEY `fk-article-category_id` (`category_id`),
  CONSTRAINT `fk-article-author_id` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE RESTRICT,
  CONSTRAINT `fk-article-category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,1,1,'New C# 8.0 Features','<p>C# 8.0 adds the following features and enhancements to the C# language:</p>\r\n<ul>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#readonly-members\" data-linktype=\"self-bookmark\">Readonly members</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#default-interface-methods\" data-linktype=\"self-bookmark\">Default interface methods</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#more-patterns-in-more-places\" data-linktype=\"self-bookmark\">Pattern matching enhancements</a>:\r\n<ul>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#switch-expressions\" data-linktype=\"self-bookmark\">Switch expressions</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#property-patterns\" data-linktype=\"self-bookmark\">Property patterns</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#tuple-patterns\" data-linktype=\"self-bookmark\">Tuple patterns</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#positional-patterns\" data-linktype=\"self-bookmark\">Positional patterns</a></li>\r\n</ul>\r\n</li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#using-declarations\" data-linktype=\"self-bookmark\">Using declarations</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#static-local-functions\" data-linktype=\"self-bookmark\">Static local functions</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#disposable-ref-structs\" data-linktype=\"self-bookmark\">Disposable ref structs</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#nullable-reference-types\" data-linktype=\"self-bookmark\">Nullable reference types</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#asynchronous-streams\" data-linktype=\"self-bookmark\">Asynchronous streams</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#indices-and-ranges\" data-linktype=\"self-bookmark\">Indices and ranges</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#null-coalescing-assignment\" data-linktype=\"self-bookmark\">Null-coalescing assignment</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#unmanaged-constructed-types\" data-linktype=\"self-bookmark\">Unmanaged constructed types</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#stackalloc-in-nested-expressions\" data-linktype=\"self-bookmark\">Stackalloc in nested expressions</a></li>\r\n<li><a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#enhancement-of-interpolated-verbatim-strings\" data-linktype=\"self-bookmark\">Enhancement of interpolated verbatim strings</a></li>\r\n</ul>\r\n<p>C# 8.0 is supported on&nbsp;<strong>.NET Core 3.x</strong>&nbsp;and&nbsp;<strong>.NET Standard 2.1</strong>. For more information, see&nbsp;<a href=\"https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/configure-language-version\" data-linktype=\"relative-path\">C# language versioning</a>.</p>','2020-03-16 18:06:42',1),(3,3,2,'Přihlášení do aplikace','<p><strong>email</strong>: <a href=\"mailto:ivon@sssvt.cz\">ivon@sssvt.cz</a></p>\r\n<p><strong>heslo</strong>: 123456a+</p>','2020-03-16 19:49:48',1);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,'Alexandr Hájek','alexax578@gmail.com','$2y$13$gBT.3gNqrluknH98sMmuIeXSeQUKSrUOLzG7Co/p/yGK3NZmeuWQW'),(3,'Roman Španko','spankoroman@sssvt.cz','$2y$13$0EYSclvh0pk9RZrIfangOOZdAbv26ht5qR1f5AIhyLJwKFMQySBxW'),(4,'Pavel Ivon','ivon@sssvt.cz','$2y$13$q1h7llUMzIcje8UNNOtdRuQi/b6iFHd3HZYCBSX5efw1WVrVyLVBq');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Programming'),(2,'Authentication');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1584374102),('m200220_093419_create_authors',1584374102),('m200220_093440_create_categories',1584374102),('m200220_093446_create_articles',1584374103);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-16 19:53:53
