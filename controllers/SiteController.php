<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use app\models\Author;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\AccessControl;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Article::find()
                ->where(['published' => 1])
                ->orderBy('created_at DESC')
                ->limit(5),
            'pagination' => false,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays login page and logs in the user.
     * 
     * @return string
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        Yii::$app->user->setReturnUrl(Yii::$app->request->referrer);

        if ($model->load(Yii::$app->request->post())) {
            $author = Author::findOne([ 'email' => $model->email ]);

            // Author with the given email does not exist - show error that user does not exist.
            if ($author === null) {
                Yii::$app->session->addFlash('danger', 'Given email does not match any existing account.');
                return $this->goBack();
            }

            if (Yii::$app->getSecurity()->validatePassword($model->password, $author->password))
            {
                Yii::$app->user->login($author);
                return $this->redirect(['index']);
            } else {
                // Given password is invalid - show error that password is invalid.
                Yii::$app->session->addFlash('danger', 'Invalid password.');
                return $this->goBack();
            }
        }

        return $this->render('login', [ 'model' => $model ]);
    }

    /**
     * Logs out current user.
     * 
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['index']);
    }
}
