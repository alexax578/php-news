<?php

use yii\db\Migration;

/**
 * Class m200220_093446_create_articles
 */
class m200220_093446_create_articles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'title' => $this->string(100)->notNull(),
            'content' => $this->text()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'published' => $this->boolean()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-article-author_id',
            'articles',
            'author_id',
            'authors',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-article-category_id',
            'articles',
            'category_id',
            'categories',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-article-author_id',
            'article'
        );

        $this->dropForeignKey(
            'fk-article-category_id',
            'article'
        );

        $this->dropTable('articles');
    }
}
