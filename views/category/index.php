<?php

use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Categories';

$dataProvider->sort = false;

?>
<div class="category-index">

    <h1 class="mb-3"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->name), ['article/list', 'categoryId' => $model->id]);
                }
            ],
            [
                'class' => ActionColumn::className(),
                'header' => 'Actions',
                'contentOptions' => array('style' => 'width: 1%; white-space: nowrap;'),
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                    },
                    'delete' => function($url, $model) {
                        return Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>