<?php

use yii\helpers\Html;

$this->title = 'Update Category: ' . $model->name;

?>
<div class="category-update">

    <h1 class="mb-3"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
