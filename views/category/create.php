<?php

use yii\helpers\Html;

$this->title = 'Create Category';

?>
<div class="category-create">

    <h1 class="mb-3"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
