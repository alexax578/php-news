<?php

use yii\widgets\ListView;

$this->title = Yii::$app->name;

?>

<div class="site-index mb-4">
    <h1 class="display-3 mb-0">Articles</h1>
    <p class="text-muted">Most recent news from IT</p>
</div>

<div>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '..\article\_small',
        'summary' => '',
    ]) ?>
</div