<?php
$this->title = Yii::$app->name;
?>
<div class="site-login">

    <h1 class="display-3">Login</h1>
    
    <?= $this->render('_loginForm', [
        'model' => $model,
    ]) ?>

</div