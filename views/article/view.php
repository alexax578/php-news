<?php

use app\models\Author;
use app\models\Category;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->title = $model->title;

\yii\web\YiiAsset::register($this);
?>
<div class="article-view">

    <h1 class="mb-1"><?= Html::encode($this->title) ?></h1>
    <div class="container">
        <div class="row">
            <?= Html::tag('p', Html::encode(date('j. F Y, G:i', strtotime($model->created_at))), ['class' => 'text-muted mr-1 mb-0']) ?>       
        </div>
        <div class="row mb-4">
            <?= Html::a(Html::encode(Author::findOne($model->author_id)->name), ['article/list', 'authorId' => $model->author_id]) ?>   
            <?= Html::a(Html::encode(Category::findOne($model->category_id)->name), ['article/list', 'categoryId' => $model->category_id], ['class' => 'ml-2']) ?>
        </div>
    </div>

    <div class="article">
        <?= HtmlPurifier::process($model->content) ?>
    </div>
</div>
