<?php

use app\helpers\ArticleHelper;
use app\models\Author;
use app\models\Category;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

?>

<div class="article-small container-sm p-0">

    <h2 class="mb-1"><?= Html::a(Html::encode($model->title), ['article/view', 'id' => $model->id]) ?></h2>
    <div class="container">
        <div class="row">
            <?= Html::tag('p', Html::encode(date('j. F Y, G:i', strtotime($model->created_at))), ['class' => 'text-muted mr-1 mb-0']) ?>       
        </div>
        <div class="row mb-3">
            <?= Html::a(Html::encode(Author::findOne($model->author_id)->name), ['article/list', 'authorId' => $model->author_id]) ?>   
            <?= Html::a(Html::encode(Category::findOne($model->category_id)->name), ['article/list', 'categoryId' => $model->category_id], ['class' => 'ml-2']) ?>
        </div>
    </div>
    <?= HtmlPurifier::process(ArticleHelper::extractIntroduction($model->content)) ?>
    <div class="container text-right">
        <?= Html::a('Read more...', ['article/view', 'id' => $model->id]) ?>
    </div>
    
</div> 