<?php

use app\models\Author;
use app\models\Category;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\SerialColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Articles';

$dataProvider->sort = false;

?>
<div class="article-index">

    <h1 class="mb-3"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Article', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => SerialColumn::className()],
            [
                'class' => DataColumn::className(),
                'header' => 'Author',
                'value' => function ($model) {
                    return Author::findOne($model->author_id)->name;
                }

            ],
            [
                'class' => DataColumn::className(),
                'header' => 'Category',
                'value' => function ($model) {
                    return Category::findOne($model->category_id)->name;
                }

            ],
            [
                'label' => 'Title',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->title, 8), ['view', 'id' => $model->id]);
                }
            ],
            [
                'class' => DataColumn::className(),
                'header' => 'Published',
                'contentOptions' => array('style' => 'width: 1%; white-space: nowrap;'),
                'value' => function ($model) {
                    return $model->published === 1 ? 'Yes' : 'No';
                }
            ],
            [
                'class' => ActionColumn::className(),
                'header' => 'Actions',
                'contentOptions' => array('style' => 'width: 1%; white-space: nowrap;'),
                'buttons' => [ 
                    'update' => function ($url, $model) {
                        return Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                    },
                    'delete' => function($url, $model) {
                        return Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
