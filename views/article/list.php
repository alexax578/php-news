<?php

use yii\widgets\ListView;

$this->title = 'articles';

?>
<div class="article-list">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_small',
    ]) ?>
</div>