<?php

namespace app\models;

class LoginForm extends \yii\base\Model
{
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['email'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 255]
        ];
    }
}
